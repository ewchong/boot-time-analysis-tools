# Boot Time Analysis Tools

A collection of tools for analyzing boot times on aarch64 platforms.

## [cntvct-log](cntvct-log)
A binary tool deployed as systemd hooks to log wall clock time stamps from power on.

## [boot_timings](boot_timings)
A python script to collect boot time log information from dbus, dmesg, and systemd-analyze and provide summary data in various formats.
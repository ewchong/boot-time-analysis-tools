# Boot Timings

The `boot_timings.py` script collects boot time log information from dbus, dmesg, and systemd. The logs are normalized and output as a `_logs.json` file. The normalized logs are also fed into the `boot_log_chart.py` script, which creates an HTML/JavaScript interactive cart output as a `_chart.html` file. Important boot time summary data that may be futher processed by automation and change detection systems is output as a `_summary.json` file. Finally, the systemd boot time data reported by dbus is structured into a hierarchical human-readable format and output to stdout.

## Python dependencies

Packages easily installed with `pip`
  - `bokeh`

These packages involve binary builds, and are easiest to install as RPMs
  - `dbus` (RPM package `python3-dbus`)
  - `systemd` (RPM package `python3-systemd`)

### Installing dependencies

Installing specific Python dependencies from the operating system's repositories rather than  `pip` avoids pulling additional dependencies.

```
sudo dnf install -y python3-pip python3-systemd python3-dbus && pip3 install bokeh
```

## Usage

Running the `boot_timings.py` script with no parameters will use a default set of KPIs for timestamps and will output all generated files into the local directory. Some parameters are available to change script behaviors:

```
$ ./boot_timings.py --help
usage: boot_timings.py [-h] [-o OUTPUT_DIRECTORY] [-d DESCRIPTION] [--kpi-re-pattern KPI_RE_PATTERN]

Collects logs and timings relevant to boot time performance.

options:
  -h, --help            show this help message and exit
  -o OUTPUT_DIRECTORY, --output-directory OUTPUT_DIRECTORY
                        Directory where output files will be stored (default: .)
  -d DESCRIPTION, --description DESCRIPTION
                        Free form description to be added to the metadata (default: None)
  --kpi-re-pattern KPI_RE_PATTERN
                        Regular expression to match log messages on for KPI highlighting (default: multi-user.target|sysinit.target|Load Kernel Modules|mounted)
```

# Outputs

## Wall clock times

The script leverages [cntvct-log](../cntvct-log/) to determine an offset value of the kernel time to the wall clock time. Timestamps reported in stdout, the logs output file, and the chart ouput file are all adjusted to wall clock time using this offset as long as the `cntvct-log` service is reporting those wall clock times to the journal. If the `cntvct-log` service is not available, the script will display a warning and the timestamps will be reported based on kernel time instead.

*Note: This script only requires a post-switchroot deployment of `cntvct-log` with the hook for `basic.target`; it does not require `cntvct-log` to be deployed in the initramfs.*

## System initialization

The reported *SystemInit* boot stage is calculated from true boot time 0 (essentially power on) to kernel time 0. This means that this stage includes all firmware time as well as time spent by the Linux kernel prior to the start of the kernel clock. **Note that this includes the memory initialization phase of the kernel.**

## Metadata

Various metadata are collected from the system and are included in the output files. A free form text description can be added by the user to the metadata by passing a string to the `--description` flag.

## Timeline

The Timeline is an ASCII representation of the boot stages with relative sizes. The timeline will scale to the width of the console, or will default to 100 characters if output is redirected to a file. Each boot stage is labeled with its duration.

## Durations

The Durations are a table representation of key boot stages with start and end times. The columns include a tab character separator, allowing for direct copy-paste into a spreadsheet.

## Summary

The Summary shows key boot stage durations and KPIs in a hierarchical layout.

## Key Performance Indicator (KPI) Timestamps

KPIs report a table of timestamps of specific logged actions during the boot process. Like the Durations, the table is copy-paste friendly for spreadsheets.

The *First Initrd Service* and *First Root Service* KPIs are built-in and will always be shown. The other KPIs are based on a regular expression pattern match of the boot logs. The default regular expression (available from the `--help` output):

  > `multi-user.target|sysinit.target|Load Kernel Modules|mounted`

The pattern match can be modified by the user at run time with a valid regular expression string passed to the `--kpi-re-pattern` flag, which will result in overriding the default pattern above.

Example:

```
# ./boot_timings.py --kpi-re-pattern "ttyS[0-1]\."
...
## KPI Timestamps (ms)
KPI                                               	Timestamp
First Initrd Service Started At 20.958675 ms      	20958.675
First Root Service Started At 23.145611 ms        	23145.611
sys-devices-platform-serial8250-tty-ttyS0.dev…    	21533.939
dev-ttyS0.device                                  	21533.942
sys-devices-platform-serial8250-tty-ttyS1.dev…    	21534.463
dev-ttyS1.device                                  	21534.464
...
```

KPIs also affect the `_logs.json` and the `_chart.html` output files. Logs matching the pattern will have a `"kpi": "True"` value added to them in the `_logs.json` file, and will be highlighted in the graphical `_chart.html` file.

## Artifacts

Output files are generated by the script in order to support multiple use cases of data analysis.

  - `_summary.json` - A machine-readable summary of important durations and timestamps in milliseconds
  - `_log.json` - A machine-readable list of all captured logs and their timing information
  - `_chart.html` - A self-contained and interactive HTML/JavaScript chart of all captured logs plotted with their start times and durations

# Example
```
# ./boot_timings.py -o /tmp

NOTE: Timestamps are in wall clock time.
The SystemInit stage includes everything up to the point that the kernel timer starts.

# System boot time analysis

## Timeline
 SystemInit: 2909ms
                 Kernel: 446ms
                   InitRD: 1783ms
                             Switchroot: 9331ms
|===============/=/=========/==================================================/=>


## Durations (ms)
Stage               	Start               	End
SystemInit           	0.0                 	2908.797
Kernel              	2908.797            	3354.631
InitRD              	3354.631            	5138.05
InitRDSystemdInit   	3354.631            	3442.413
InitRDSecurity      	3355.454            	3355.598
InitRDGenerators    	3386.193            	3438.885
InitRDUnitsLoad     	3438.891            	3442.413
Switchroot           	5138.05             	14468.862
SystemdInit         	5138.05             	5503.297
Security            	5138.848            	5267.386
Generators          	5409.174            	5430.406
UnitsLoad           	5430.413            	5503.297


## Summary
SystemInit: 2908.797 ms
Kernel: 445.834 ms
InitRD: 1783.419 ms
  InitRDSystemdInit: 87.782 ms
    InitRDSecurity: 0.144 ms
    InitRDGenerators: 52.692 ms
    InitRDUnitsLoad: 3.522 ms
  First Initrd Service Started At 3.442413 ms
Switchroot: 9330.812 ms
  SystemdInit: 365.247 ms
    Security: 128.538 ms
    Generators: 21.232 ms
    UnitsLoad: 72.884 ms
  First Root Service Started At 5.503297 ms
Overall: 14468.862 ms
NInstalledJobs: 186
NFailedJobs: 22


## KPI Timestamps (ms)
KPI                                               	Timestamp
First Initrd Service Started At 3.442413 ms       	3442.413
First Root Service Started At 5.503297 ms         	5503.297
systemd[1]: Starting Load Kernel Modules...       	3478.936
systemd[1]: Finished Load Kernel Modules.         	3492.297
EXT4-fs (sde38): mounted filesystem 76a22bf4-…    	4912.188
systemd[1]: Starting Load Kernel Modules...       	5777.737
EXT4-fs (sde38): re-mounted 76a22bf4-f153-454…    	5794.043
systemd[1]: Finished Load Kernel Modules.         	5794.397
sysinit.target                                    	7312.478
multi-user.target                                 	8418.628


## Artifacts
Created summary JSON file at /tmp/example_03_18_2024_22_59_06-boot_time_summary.json
Created log JSON file at /tmp/example_03_18_2024_22_59_06-boot_time_logs.json
Created log chart at /tmp/example_03_18_2024_22_59_06-boot_time_chart.html
```

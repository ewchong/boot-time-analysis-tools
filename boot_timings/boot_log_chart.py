"""Generates an interactive chart of log message timings"""

import copy

from bokeh.plotting import figure, save
from bokeh.models import ColumnDataSource, HoverTool, LabelSet
from bokeh.io import output_file


def build_chart(timings, metadata, basename):
    """Main function for building the chart"""
    # Parallel list data structure to be used by Bokeh ColumDataSource
    data = {
        "id": [],
        "start": [],
        "duration": [],
        "end": [],
        "name": [],
        "label": [],
        "source": [],
        "color": [],
        "bar_height": [],
    }

    # Copy of the data structure to hold the sorted lists
    sorted_data = copy.deepcopy(data)

    # Standard visualization formatting
    # Colors adjusted for colorblindness accessibility
    # dmesg_color = "blue"
    dmesg_color = "#0C7BDC"
    # systemd_color = "red"
    systemd_color = "#D41159"
    # dbusDolor = "orange"
    dbus_color = "#FFC20A"
    standard_bar_height = 1
    chart_width = 1000
    chart_height = 700

    # Highlight formatting for KPIs
    # highlight_color = "green"
    highlight_color = "#37C0D5"
    highlight_bar_height = 2

    # Parse the boot-time sample data structure for metrics of interest
    for item in timings:
        if "SystemInit" not in item["name"]:
            data["start"].append(round(item["activating"] / 1000, 3))
            data["duration"].append(round(item["time"] / 1000, 3))
            data["end"].append(round((item["activating"] + item["time"]) / 1000, 3))
            data["name"].append(item["name"])
            data["source"].append(item["source"])
            if "kpi" in item and item["kpi"]:
                data["color"].append(highlight_color)
                data["bar_height"].append(highlight_bar_height)
                data["label"].append(item["name"])
            else:
                if item["source"] == "saplot":
                    data["color"].append(systemd_color)
                elif item["source"] == "systemd-dbus":
                    data["color"].append(dbus_color)
                else:
                    data["color"].append(dmesg_color)
                data["bar_height"].append(standard_bar_height)
                data["label"].append(None)

    # Parallel-sort the data structure lists based on start time
    (
        sorted_data["start"],
        sorted_data["duration"],
        sorted_data["end"],
        sorted_data["name"],
        sorted_data["label"],
        sorted_data["source"],
        sorted_data["color"],
        sorted_data["bar_height"],
    ) = (
        list(t)
        for t in zip(
            *sorted(
                zip(
                    data["start"],
                    data["duration"],
                    data["end"],
                    data["name"],
                    data["label"],
                    data["source"],
                    data["color"],
                    data["bar_height"],
                )
            )
        )
    )

    # Enumerate the log items sequentially after sort
    for item_id, _ in enumerate(sorted_data["start"]):
        sorted_data["id"].append(item_id)

    # Get total number of actions
    total_actions = len(sorted_data["id"])

    # Convert the data into a ColumnDataSource for Bokeh
    source = ColumnDataSource(data=sorted_data)

    # Configure the tool-tip display on mouse hover
    hover = HoverTool(
        tooltips=[
            ("Source", "@source"),
            ("Name", "@name"),
            ("Started", "@start{0.00} ms"),
            ("Ended", "@end{0.00} ms"),
            ("Run Time", "@duration{0.00} ms"),
        ],
        point_policy="follow_mouse",
    )

    chart_title = f"Boot Time Measurements -- {total_actions} Logged Actions\n\n"
    for key, value in metadata.items():
        chart_title += f"{key}: {value}\n"

    # Build the Bokeh chart
    p = figure(
        title=chart_title,
        y_axis_label="Boot Action (Sequence ID)",
        x_axis_label="Time Since Start (ms)",
        width=chart_width,
        height=chart_height,
    )
    p.hbar(
        y="id",
        left="start",
        right="end",
        source=source,
        color="color",
        height="bar_height",
    )
    p.tools.append(hover)
    p.y_range.flipped = True
    p.title.text_font_size = "15pt"
    p.axis.major_label_text_font_size = "15pt"
    p.axis.axis_label_text_font_size = "15pt"

    # Label the highlighted items
    labels = LabelSet(
        x="start",
        y="id",
        text="label",
        x_offset=5,
        y_offset=5,
        source=source,
        text_color=highlight_color,
        background_fill_color="white",
        border_line_color=highlight_color,
        border_line_width=1,
    )
    p.add_layout(labels)

    output_filename = f"{basename}-boot_time_chart.html"
    output_file(filename=output_filename)
    save(p)

    return f"Created log chart at {output_filename}"

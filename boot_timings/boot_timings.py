#!/usr/bin/env python3
"""Collects logs and timings relevant to boot time performance."""

from typing import Tuple
from datetime import datetime
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import subprocess
import logging
import sys
import re
import ast
import json
import socket
import platform
import io
import os
import dbus
from systemd import journal
import boot_log_chart


# Calculate the wall clock offset from the cntvct log value
# Get CNTVCT_OFFSET as a global variable
try:
    j = journal.Reader()
    j.this_boot()
    j.add_match(UNIT="basic.target")
    j.seek_tail()
    basic_target_timestamp = j.get_previous()["__MONOTONIC_TIMESTAMP"][0].total_seconds()


    j.flush_matches()
    j.add_match(_SYSTEMD_UNIT="cntvct@basic.service")
    basic_target_cntvct = float(j.get_next()["MESSAGE"].strip("s."))

    # Derive the kernel clock to wall clock offset in nanoseconds
    CNTVCT_OFFSET = (basic_target_cntvct - basic_target_timestamp) * 1000000
    
    print("\nNOTE: Timestamps are in wall clock time.")
    print("The SystemInit stage includes everything up to the point that the kernel timer starts.")

except KeyError:
    CNTVCT_OFFSET = 0
    
    print("\nWARNING: Unable to find a required journal match for cntvct time.")
    print("Times will be reported in kernel time instead of wall clock time.")


# Parse arguments
def directory_path(string):
    """Confirms a given path is a directory"""
    if os.path.isdir(string):
        return string
    raise NotADirectoryError(string)


parser = ArgumentParser(
    description="Collects logs and timings relevant to boot time performance.",
    formatter_class=ArgumentDefaultsHelpFormatter
)
parser.add_argument(
    "-o",
    "--output-directory",
    type=directory_path,
    default=".",
    help="Directory where output files will be stored",
)
parser.add_argument(
    "-d", "--description", type=str, help="Free form description to be added to the metadata"
)
parser.add_argument(
    "--kpi-re-pattern",
    type=str,
    default="multi-user.target|sysinit.target|Load Kernel Modules|mounted",
    help="Regular expression to match log messages on for KPI highlighting",
)

try:
    args = parser.parse_args()
except NotADirectoryError:
    print("Target output directory path does not exist\n")
    sys.exit(1)


def run_command(command: str, timeout: int = 60) -> Tuple[int, str, str]:
    """Run a shell command and return a Tuple of the return code, stdout, and stderr"""
    # Split the command string to a list
    command_list = command.split()

    try:
        with subprocess.Popen(
            command_list,
            text=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        ) as process:
            stdout, stderr = process.communicate(timeout=timeout)
    except subprocess.CalledProcessError as e:
        logging.error("Shell command failed: %e", e)
        sys.exit(1)
    except subprocess.TimeoutExpired as e:
        logging.error("Shell command timed out: %e", e)
        sys.exit(1)

    exit_status = process.returncode

    return (exit_status, stdout, stderr)


def get_time_diff(props, label, parent, start_key, finish_key, offset=CNTVCT_OFFSET):
    """Calculate and return the time difference of start and finish keys"""
    if start_key not in props:
        return {}

    finish = props[finish_key] + offset
    start = props[start_key] + offset
    diff = finish - start
    if diff == 0:
        return {}

    props_dict = {
        "source": "systemd-dbus",
        "name": label,
        "activating": start,
        "activated": finish,
        "time": diff,
    }

    if parent:
        props_dict["parent"] = parent

    return props_dict


def get_time_diff_for_key_prefix(props, key_prefix, parent):
    """Derive start and finish keys for logged items that use prefixes"""
    start_key = f"{key_prefix}StartTimestampMonotonic"
    finish_key = f"{key_prefix}FinishTimestampMonotonic"
    props_dict = get_time_diff(props, key_prefix, parent, start_key, finish_key)
    return props_dict


def log_filter(
    log_item: dict,
    max_start_time,
    kpi_re_pattern: str = args.kpi_re_pattern,
):
    """Apply filters to the log structures"""
    if log_item["activating"] < max_start_time or re.search(kpi_re_pattern, log_item["name"]):
        if re.search(kpi_re_pattern, log_item["name"]):
            log_item["kpi"] = True
        # Apply the wall clock time offset
        log_item["activating"] = log_item["activating"] + CNTVCT_OFFSET
        if "activated" in log_item.keys():
            log_item["activated"] = log_item["activated"] + CNTVCT_OFFSET
        if "deactivating" in log_item.keys():
            log_item["deactivating"] = log_item["deactivating"] + CNTVCT_OFFSET
        if "deactivated" in log_item.keys():
            log_item["deactivated"] = log_item["deactivated"] + CNTVCT_OFFSET
        return log_item
    # DEBUG
    # print(f"filtered {log_item}")
    # sys.exit()
    return None


def print_systemd_stages(input_list: list):
    """Print the systemd stages and timings hierarchy in a human-readable format"""
    output_dict = {}
    for item in input_list:
        if "count" in item.keys():
            output_dict[item["name"]] = {"count": item["count"]}
            continue
        output_dict[item["name"]] = {"duration": item["time"]}
        if "parent" in item.keys():
            output_dict[item["name"]]["parent"] = item["parent"]

    try:
        console_width, _ = os.get_terminal_size()
    except OSError:
        console_width = 100
    timeline_max_length = console_width - 6
    overall_duration = output_dict["Overall"]["duration"]

    # Calculate the portion lengths for the ASCII timeline
    sysinit_length = round(
        ((output_dict["SystemInit"]["duration"] / overall_duration) * timeline_max_length) - 1
    )
    kernel_length = round(
        ((output_dict["Kernel"]["duration"] / overall_duration) * timeline_max_length) - 1
    )
    initrd_length = round(
        ((output_dict["InitRD"]["duration"] / overall_duration) * timeline_max_length) - 1
    )
    switchroot_length = round(
        ((output_dict["Switchroot"]["duration"] / overall_duration) * timeline_max_length) - 1
    )

    # Set the label strings for the ASCII timeline
    sysinit_string = f"SystemInit: {round(output_dict['SystemInit']['duration'] / 1000)}ms"
    kernel_string = f"Kernel: {round(output_dict['Kernel']['duration'] / 1000)}ms"
    initrd_string = f"InitRD: {round(output_dict['InitRD']['duration'] / 1000)}ms"
    switchroot_string = f"Switchroot: {round(output_dict['Switchroot']['duration'] / 1000)}ms"

    # Print the labels with appropriate spacing
    formatted_string = "## Timeline\n"
    formatted_string += " " + sysinit_string
    sysinit_gap = sysinit_length - len(sysinit_string)
    if sysinit_gap < 1:
        formatted_string += "\n" + " " * (sysinit_length + 2)
    else:
        formatted_string += " " * (sysinit_gap + 1)
    formatted_string += kernel_string
    kernel_gap = kernel_length - len(kernel_string)
    if kernel_gap < 1:
        formatted_string += "\n" + " " * (sysinit_length + kernel_length + 3)
    else:
        formatted_string += " " * (kernel_gap + 1)
    formatted_string += initrd_string
    initrd_gap = initrd_length - len(initrd_string)
    if initrd_gap < 1:
        formatted_string += "\n" + " " * (sysinit_length + kernel_length + initrd_length + 4)
    else:
        formatted_string += " " * ((initrd_length - len(initrd_string)) + 1)
    formatted_string += switchroot_string + "\n"

    # Print the ASCII timeline
    formatted_string += (
        "|"
        + "=" * sysinit_length
        + "/"
        + "=" * kernel_length
        + "/"
        + "=" * initrd_length
        + "/"
        + "=" * switchroot_length
        + "/=>\n\n"
    )

    formatted_string += f"\n## Durations (ms)\n{'Stage':<20}\t{'Start':<20}\tEnd\n"
    for stage in input_list:
        if "activated" in stage.keys():
            formatted_string += f"{stage['name']:<20}"
            formatted_string += f"\t{stage['activating'] / 1000:<20}"
            formatted_string += f"\t{stage['activated'] / 1000}\n"

    formatted_string += "\n\n## Summary\n" + organize_hierarchy(output_dict) + "\n"

    return formatted_string


def organize_hierarchy(input_dict, parent_key=None, indent=0):
    """Recursive function to create the hierarchy from the parent keys"""
    hierarchy_str = ""
    for key, value in input_dict.items():
        if value.get("parent") == parent_key:
            if value.get("count"):
                metric = f"{str(value.get('count', ''))}\n"
            elif "Service" in key:
                metric = "\n"
            else:
                metric = f"{str(value.get('duration', 0) / 1000)} ms\n"
            hierarchy_str += " " * indent + key
            hierarchy_str += ": " if ("Service" not in key) else ""
            hierarchy_str += metric
            children_str = organize_hierarchy(input_dict, key, indent + 2)
            hierarchy_str += children_str
    return hierarchy_str


def kpi_table(logs):
    """Returns a tab-separated table of KPIs and timestamps"""
    table = f"## KPI Timestamps (ms)\n{'KPI':<50}\tTimestamp\n"
    for log in logs:
        if "kpi" in log.keys():
            table += f"{(log['name'][:45] + '…' if len(log['name']) > 45 else log['name']):<50}"
            table += f"\t{log['activating'] / 1000:>}\n"
    return table


def main():
    """Main program function"""

    # Get dbus timings for systemd
    system_bus = dbus.SystemBus()
    systemd_properties = system_bus.call_blocking(
        "org.freedesktop.systemd1",
        "/org/freedesktop/systemd1",
        "org.freedesktop.DBus.Properties",
        "GetAll",
        "s",
        [""],
    )

    # DEBUG
    # print(json.dumps(systemd_properties, indent=1))

    # Capture machine-readable hierarchy of stages
    dbus_list = []
    satime_dict = {}
    initramfs_dict = {}
    switchroot_dict = {}

    # Note: We can't consistently get Firmware and Loader times from all platforms
    # dbus_list.append(
    #     get_time_diff(
    #         systemd_properties,
    #         "Firmware",
    #         None,
    #         "FirmwareTimestampMonotonic",
    #         "LoaderTimestampMonotonic",
    #     )
    # )
    # dbus_list.append(
    #     get_time_diff(
    #         systemd_properties,
    #         "Loader",
    #         None,
    #         "LoaderTimestampMonotonic",
    #         "LoaderTimestampMonotonic",
    #     )
    # )
    sysinit_timing = {
        "source": "systemd-dbus",
        "name": "SystemInit",
        "activating": 0,
        "activated": CNTVCT_OFFSET,
        "time": CNTVCT_OFFSET,
    }
    dbus_list.append(sysinit_timing)
    satime_dict["sysinit"] = sysinit_timing["time"] / 1000000
    kernel_timing = get_time_diff(
        systemd_properties,
        "Kernel",
        None,
        "KernelTimestampMonotonic",
        "InitRDTimestampMonotonic",
    )
    dbus_list.append(kernel_timing)
    satime_dict["kernel"] = kernel_timing["time"] / 1000000
    initrd_timing = get_time_diff(
        systemd_properties,
        "InitRD",
        None,
        "InitRDTimestampMonotonic",
        "UserspaceTimestampMonotonic",
    )
    dbus_list.append(initrd_timing)
    satime_dict["initrd"] = initrd_timing["time"] / 1000000
    initrd_systemd_init_timing = get_time_diff(
        systemd_properties,
        "InitRDSystemdInit",
        "InitRD",
        "InitRDTimestampMonotonic",
        "InitRDUnitsLoadFinishTimestampMonotonic",
    )
    dbus_list.append(initrd_systemd_init_timing)
    initramfs_dict["systemd_init"] = initrd_systemd_init_timing["time"] / 1000000
    initrd_security_timing = get_time_diff_for_key_prefix(
        systemd_properties, "InitRDSecurity", "InitRDSystemdInit"
    )
    dbus_list.append(initrd_security_timing)
    initramfs_dict["security"] = initrd_security_timing["time"] / 1000000
    initrd_generators_timing = get_time_diff_for_key_prefix(
        systemd_properties, "InitRDGenerators", "InitRDSystemdInit"
    )
    dbus_list.append(initrd_generators_timing)
    initramfs_dict["generators"] = initrd_generators_timing["time"] / 1000000
    initrd_units_load_timing = get_time_diff_for_key_prefix(
        systemd_properties, "InitRDUnitsLoad", "InitRDSystemdInit"
    )
    dbus_list.append(initrd_units_load_timing)
    initramfs_dict["units_load"] = initrd_units_load_timing["time"] / 1000000
    initrd_service_start_ts = (
        systemd_properties["InitRDUnitsLoadFinishTimestampMonotonic"] + CNTVCT_OFFSET
    )
    initrd_first_service_timestamp = {
        "source": "systemd-dbus",
        "name": f"First Initrd Service Started At {initrd_service_start_ts / 1000000} ms",
        "time": 0,
        "activating": initrd_service_start_ts,
        "parent": "InitRD",
        "kpi": "True",
    }
    dbus_list.append(initrd_first_service_timestamp)
    initramfs_dict["first_service_ts"] = initrd_first_service_timestamp["activating"] / 1000000
    switchroot_timing = get_time_diff(
        systemd_properties,
        "Switchroot",
        None,
        "UserspaceTimestampMonotonic",
        "FinishTimestampMonotonic",
    )
    dbus_list.append(switchroot_timing)
    satime_dict["switchroot"] = switchroot_timing["time"] / 1000000
    systemd_init_timing = get_time_diff(
        systemd_properties,
        "SystemdInit",
        "Switchroot",
        "UserspaceTimestampMonotonic",
        "UnitsLoadFinishTimestampMonotonic",
    )
    dbus_list.append(systemd_init_timing)
    switchroot_dict["systemd_init"] = systemd_init_timing["time"] / 1000000
    security_timing = get_time_diff_for_key_prefix(systemd_properties, "Security", "SystemdInit")
    dbus_list.append(security_timing)
    switchroot_dict["security"] = security_timing["time"] / 1000000
    generators_timing = get_time_diff_for_key_prefix(
        systemd_properties, "Generators", "SystemdInit"
    )
    dbus_list.append(generators_timing)
    switchroot_dict["generators"] = generators_timing["time"] / 1000000
    units_load_timing = get_time_diff_for_key_prefix(systemd_properties, "UnitsLoad", "SystemdInit")
    dbus_list.append(units_load_timing)
    switchroot_dict["units_load"] = units_load_timing["time"] / 1000000
    root_service_start_ts = systemd_properties["UnitsLoadFinishTimestampMonotonic"] + CNTVCT_OFFSET
    first_service_timestamp = {
        "source": "systemd-dbus",
        "name": f"First Root Service Started At {root_service_start_ts / 1000000} ms",
        "time": 0,
        "activating": root_service_start_ts,
        "parent": "Switchroot",
        "kpi": "True",
    }
    dbus_list.append(first_service_timestamp)
    switchroot_dict["first_service_ts"] = first_service_timestamp["activating"] / 1000000
    overall_timing = {
        "source": "systemd-dbus",
        "name": "Overall",
        "time": systemd_properties["FinishTimestampMonotonic"] + CNTVCT_OFFSET,
        "activating": 0,
        "ignore": "True",
    }
    dbus_list.append(overall_timing)
    satime_dict["total"] = overall_timing["time"] / 1000000
    dbus_list.append(
        {
            "source": "systemd-dbus",
            "name": "NInstalledJobs",
            "count": systemd_properties["NInstalledJobs"],
            "ignore": "True",
        }
    )
    dbus_list.append(
        {
            "source": "systemd-dbus",
            "name": "NFailedJobs",
            "count": systemd_properties["NFailedJobs"],
            "ignore": "True",
        }
    )

    # Set the max time used for filtering other logs
    max_time = systemd_properties["FinishTimestampMonotonic"] + CNTVCT_OFFSET

    # DEBUG
    # print(json.dumps(systemd_properties, indent=1))
    # print(json.dumps(dbus_list, indent=1))

    # Friendly print systemd stages from list
    print("\n# System boot time analysis\n")
    print(print_systemd_stages(dbus_list))

    # Get dmesg logs
    dmesg_exit, dmesg_out, dmesg_err = run_command("dmesg -d")

    if dmesg_exit:
        logging.error("dmesg command failed: %e", dmesg_err)
        sys.exit(1)

    # Parse dmesg log messages and timings
    dmesg_lines = dmesg_out.splitlines()

    dmesg_list = []

    for line in dmesg_lines:
        log = re.split("] | <", line, maxsplit=2)
        try:
            dmesg_dict = {}
            dmesg_dict["source"] = "dmesg"
            dmesg_dict["name"] = log[2]
            dmesg_dict["activating"] = int(float(log[0].strip("[ ")) * 1000000)
            dmesg_dict["time"] = int(float(log[1].strip(">")) * 1000000)
            filter_check = log_filter(dmesg_dict, max_time)
            if filter_check:
                dmesg_list.append(filter_check)
        except IndexError:
            continue

    # DEBUG
    # print(dmesg_list)

    # Get systemd-analyze plot logs
    saplot_exit, saplot_out, saplot_error = run_command(
        "systemd-analyze plot --json=short --no-legend"
    )

    saplot_list = []

    if saplot_exit:
        logging.warning("systemd-analyze plot command failed: %e", saplot_error)

    # Parse systemd-analyze plot log messages and timings
    else:
        saplot_dict = ast.literal_eval(saplot_out)
        for log in saplot_dict:
            log["source"] = "saplot"
            filter_check = log_filter(log, max_time)
            if filter_check:
                saplot_list.append(filter_check)

    # DEBUG
    # print(saplot_list)

    timestamp = datetime.now()

    metadata = {}
    metadata["hostname"] = socket.gethostname()
    metadata["date"] = timestamp.isoformat()
    metadata["platform"] = platform.platform(terse=True)
    metadata["kernel"] = platform.release()
    metadata["model"] = platform.processor()
    metadata["architecture"] = platform.machine()
    metadata["system"] = platform.system()
    metadata["wall_clock_offset_ns"] = CNTVCT_OFFSET

    # DEBUG
    # print(json.dumps(metadata, indent=2))

    # Merge logs
    all_logs = dbus_list + dmesg_list + saplot_list

    # Remove items marked to ignore
    cleaned_logs = []
    for log in all_logs:
        if "ignore" not in log.keys():
            cleaned_logs.append(log)

    # Debug
    # print(json.dumps(cleaned_logs, indent=1))

    # Print a table of KPIs and timestamps
    print(kpi_table(cleaned_logs))

    logs_dict = {
        "metadata": metadata,
        "boot_logs": cleaned_logs,
    }

    # Merge summary item
    summary_dict = {}
    summary_dict["system_config"] = metadata
    summary_dict["satime"] = satime_dict
    summary_dict["initramfs"] = initramfs_dict
    summary_dict["switchroot"] = switchroot_dict
    if args.description:
        summary_dict["system_config"]["description"] = args.description

    # DEBUG
    # print(json.dumps(summary_dict, indent=1))

    print("\n## Artifacts")

    file_prefix = f"{args.output_directory}/"
    file_prefix += f"{metadata['hostname']}_{timestamp.strftime('%m_%d_%Y_%H_%M_%S').strip()}"

    # Output the summary data as JSON
    summary_output_file = f"{file_prefix}-boot_time_summary.json"
    with io.open(summary_output_file, "w", encoding="utf8") as out_file:
        summary_string = json.dumps(
            summary_dict,
            indent=4,
            sort_keys=False,
            separators=(",", ": "),
            ensure_ascii=False,
        )
        out_file.write(f"{summary_string}\n")
        print(f"Created summary JSON file at {summary_output_file}")

    # Output the log data as JSON
    log_output_file = f"{file_prefix}-boot_time_logs.json"
    with io.open(log_output_file, "w", encoding="utf8") as out_file:
        logs_string = json.dumps(
            logs_dict,
            indent=4,
            sort_keys=False,
            separators=(",", ": "),
            ensure_ascii=False,
        )
        out_file.write(f"{logs_string}\n")
        print(f"Created log JSON file at {log_output_file}")

    # Build and export the log chart
    print(
        boot_log_chart.build_chart(
            cleaned_logs,
            metadata,
            file_prefix,
        )
    )


if __name__ == "__main__":
    main()

#include <getopt.h>
#include <errno.h>
#include <stdio.h>

static inline unsigned long cntvct_read(void)
{
	unsigned long cntvct;

	asm volatile("isb\n mrs %0, cntvct_el0"
		: "=r"(cntvct)
		:: "memory");

	return cntvct;
}

static inline unsigned long cntfrq_read(void)
{
	unsigned long cntfrq;

	asm volatile("isb\n mrs %0, cntfrq_el0"
		: "=r"(cntfrq)
		:: "memory");

	return cntfrq;
}

static inline int usage(FILE *o, int rc)
{
	fprintf(o,
		"Usage: cntvct [-h] [-m|-u]\n"
		"Prints the value of cntvct (arm,aarch64) in seconds.\n\n"
		"	-h display this usage and exit.\n"
		"	-m convert cntvct value to milliseconds.\n"
		"	-r reads cntvct raw value.\n"
		"	-u convert cntvct value to microseconds.\n");
	return rc;
}

enum unit {
	RAW = 0,
	USEC,
	MSEC,
	SEC,
};

static void cntvct_print(enum unit u)
{
	unsigned long cntfrq = cntfrq_read();
	unsigned long cntvct = cntvct_read();

	switch (u) {
		case USEC:
			printf("%luus.\n", cntvct / (cntfrq / 1000000));
			break;
		case MSEC:
			printf("%lums.\n", cntvct / (cntfrq / 1000));
			break;
		case SEC:
			printf("%.6fs.\n", (double)cntvct / cntfrq);
			break;
		case RAW:
		default:
			printf("%lu\n", cntvct);
	}
}

int main(int argc, char *argv[])
{
	int opt;
	enum unit unit = SEC;

	while ((opt = getopt(argc, argv, "hmru")) != -1) {
		switch (opt) {
			case 'h':
				return usage(stdout, 0);
			case 'm':
				unit = MSEC;
				break;
			case 'r':
				unit = RAW;
				break;
			case 'u':
				unit = USEC;
				break;
			default:
				return usage(stderr, EINVAL);
		}
	}

	cntvct_print(unit);

	return 0;
}
